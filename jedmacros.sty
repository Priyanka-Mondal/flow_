% Environment for centring text without adding vertical space around it.
\newenvironment{nscenter}
  {\parskip=0pt\par\nopagebreak\centering}
  {\par\noindent\ignorespacesafterend}

% Tabular math environment.
\newenvironment{matharray}[1]
  {\begin{displaymath}\begin{array}{#1}}
  {\end{array}\end{displaymath}}

% Misc colours.
\newcommand\grey[1]{{\color{gray} #1}}
\newcommand\darkgrey[1]{{\color[gray]{0.4} #1}}
\newcommand\darkred[1]{{\color{Mahogany} #1}}
\newcommand\blue[1]{{\color{NavyBlue} #1}}


%%%
% Syntax
%%%

% Normalization
\newcommand\norm[3]{\ensuremath{\mathcal{N}{}_{#2}(#1, #3)}}
\WithSuffix\newcommand\norm*[2]{\ensuremath{\mathcal{N}{}_{#2}(#1)}}

% Ownership projection
\renewcommand\owns[1]{\ensuremath{\mathord{:}#1}}

% FLAM trust configuration
\newcommand\trustset{\ensuremath{\mathcal{H}}}

% Delegation.
\newcommand\del[3]{\ensuremath{%
  (#1 \!≽\! #2, #3)%
}}

% Voice of a principal.
\newcommand\voice[1]{\ensuremath{{\voice*}({#1})}}
\WithSuffix\newcommand\voice*{\ensuremath{∇}}

% View of a principal.
\newcommand\view[1]{\ensuremath{{\view*}({#1})}}
\WithSuffix\newcommand\view*{\ensuremath{Δ}}

\newcommand\whitehourglass{\mathop{\text{%
\raisebox{0.4ex}{\hbox{\scalebox{0.55}{\vbox{\hbox{\hskip -0.04ex ∇}\vskip -2.2ex \hbox{\rotatebox{180}{∇}}}}}}}}}
%\scalebox{0.85}{$\vartriangleright\mkern-4mu\vartriangleleft$}}}}

\newcommand\Vice[1]{\ensuremath{\whitehourglass #1}}


%%%
% Judgements
%%%

% Acts-for judgement
\newcommand\afjudge[3]{{#1} ⊢ {#2} ≽ {#3}}
\newcommand\notafjudge[3]{{#1} \nvdash {#2} ≽ {#3}}

% Acts-for judgement with common-case context
\WithSuffix\newcommand\afjudge*[2]{\afjudge{\PH;c;\pc;ℓ}{#1}{#2}}
\WithSuffix\newcommand\notafjudge*[2]{\notafjudge{\PH;c;\pc;ℓ}{#1}{#2}}

\newcommand\reqjudge[3]{\ensuremath{{#1} ⊩ {#2} ≡ {#3}}}
\newcommand\notreqjudge[3]{\ensuremath{{#1} ⊩ {#2} \not≡ {#3}}}
\WithSuffix\newcommand\reqjudge*[2]{\reqjudge{Π;\pc;\pc}{#1}{#2}}
\WithSuffix\newcommand\notreqjudge*[2]{\notreqjudge{Π;\pc;\pc}{#1}{#2}}

% Acts-for robust judgement
\newcommand\rafjudge[3]{%
  \ensuremath{{#1} \Vdash {#2} ≽ {#3}}%
}
\newcommand\notrafjudge[3]{\ensuremath{{#1} \nVdash {#2} ≽ {#3}}}

% Acts-for robust judgement with common-case context
\WithSuffix\newcommand\rafjudge*[2]{\rafjudge{Π;\pc;\pc}{#1}{#2}}
\WithSuffix\newcommand\notrafjudge*[2]{\notrafjudge{Π;\pc;\pc}{#1}{#2}}

% Speaks-for robust judgement
\newcommand\rsfjudge[3]{%
  \ensuremath{{#1} \Vdash {#2} ⇒ {#3}}%
}
\newcommand\notrsfjudge[3]{%
  \ensuremath{{#1} \nVdash {#2} ⇒ {#3}}%
}

% Speaks-for robust judgement with common-case context
\WithSuffix\newcommand\rsfjudge*[2]{\rsfjudge{\PH;c;\pc;ℓ}{#1}{#2}}
\WithSuffix\newcommand\notrsfjudge*[2]{\notrsfjudge{\PH;c;\pc;ℓ}{#1}{#2}}

% Flows-to judgement
\newcommand\flowjudge[3]{%
  \ensuremath{{#1} ⊢ {#2} ⊑ {#3}}%
}

% Flows-to judgement (abstract context)
\WithSuffix\newcommand\flowjudge*[2]{\flowjudge{Π;\pc;\pc}{#1}{#2}}

% Flows-to robustly judgement
\newcommand\rflowjudge[3]{%
  \ensuremath{{#1} \Vdash {#2} ⊑ {#3}}%
}
\newcommand\notrflowjudge[3]{{#1} \nVdash {#2} ⊑ {#3}}

% Flows-to robustly judgement (abstract context)
\WithSuffix\newcommand\rflowjudge*[2]{\rflowjudge{Π;\pc;\pc}{#1}{#2}}
\WithSuffix\newcommand\notrflowjudge*[2]{\notrflowjudge{Π;\pc;\pc}{#1}{#2}}


%%%
% Inference rules
%%%

% Acts-for rule (abstract context)
%  #1 - context (defaults to \PH;c;\pc;ℓ)
%  #2 - rule name
%  #3 - premises
%  #4 - superior in consequent
%  #5 - inferior in consequent
\newcommand\afrule[5][\PH;c;\pc;ℓ]{
  \afrule*[#1]{#2}{#3}{#4}{#5}{}
}

% Robustly acts-for rule
\newcommand\rafrule[5][\PH;c;\pc;ℓ]{
  \rafrule*[#1]{#2}{#3}{#4}{#5}{}
}

% Acts-for rule (abstract context, with side condition)
%  #1 - context (defaults to \PH;c;\pc;ℓ)
%  #2 - rule name
%  #3 - premises
%  #4 - superior in consequent
%  #5 - inferior in consequent
%  #6 - side condition
\WithSuffix\newcommand\afrule*[6][\PH;c;\pc;ℓ]{
  [\textsc{#2}]~
  \label{rule:#2}
  \hfill
  \ensuremath{\inferrule{#3}{\afjudge{#1}{#4}{#5}}}
  \ifthenelse{\equal{#6}{}}{}{\quad (#6)}
  \hfill
}

% Robustly acts-for rule (with side condition)
\WithSuffix\newcommand\rafrule*[6][\PH;c;\pc;ℓ]{
  [\textsc{#2}]
  \label{rule:#2}
  \hfill
  \ensuremath{\inferrule{#3}{\rafjudge{#1}{#4}{#5}}}
  \ifthenelse{\equal{#6}{}}{}{\quad (#6)}
  \hfill
}

% Robustly speaks-for rule (abstract context)
%  #1 - context (defaults to \PH;c;\pc;ℓ)
%  #2 - rule name
%  #3 - premises
%  #4 - p part of "p⇒q" in consequent
%  #5 - q part of "p⇒q" in consequent
\newcommand\rsfrule[5][\PH;c;\pc;ℓ]{
  [\textsc{#2}]
  \label{rule:#2}
  \hfill
  \ensuremath{\inferrule{#3}{\rsfjudge{#1}{#4}{#5}}}
  \hfill
}

% For referencing an inference rule.
\newcommand\ruleref[1]{%
  \@ifundefined{r@rule:#1}{%
    \textsc{#1} ({\bf ??})%
  }{%
    %\hyperref[rule:#1]{\textsc{#1}}%
    {\textsc{#1}}%
  }%
}
